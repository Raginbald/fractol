/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 15:41:52 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/06 15:37:21 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "fractol.h"

static void	ft_free_tab(t_tab_param *tab)
{
	int	i;

	i = -1;
	while (++i < 3)
	{
		free(tab[i].name);
		tab[i].name = NULL;
	}
	free(tab);
	tab = NULL;
}

static void	ft_end(char *str, int fd, int flag)
{
	ft_putendl_fd(str, fd);
	exit(flag);
}

static void	ft_end_env(t_env *env, char *str, int fd, int flag)
{
	if (ft_check_init(env))
	{
		ft_free_tab(env->tab_param);
		free(env);
		ft_end(str, fd, flag);
	}
	else
	{
		free(env);
		ft_end(str, fd, flag);
	}
}

static void	ft_end_env_mlx(t_env *env, char *str, int fd, int flag)
{
	mlx_clear_window(INI, WIN);
	mlx_destroy_window(INI, WIN);
	ft_end_env(env, str, fd, flag);
}

void		ft_exit(t_env *env, int flag, int fd, char *exit_str)
{
	if (!env)
		ft_end(exit_str, fd, flag);
	if (flag)
		ft_end_env(env, exit_str, fd, flag);
	else
		ft_end_env_mlx(env, exit_str, fd, flag);
}
