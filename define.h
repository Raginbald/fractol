/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   define.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 19:13:05 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 13:27:08 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINE_H
# define DEFINE_H

# include "struct.h"

# define ON				2
# define OFF			1
# define C 				0.356
# define CTE2			0.75
# define PIX_WIN		0.004
# define WIDTH			1000
# define HEIGHT			1000
# define MID_WIDTH		500
# define MID_HEIGHT		500
# define RIGHT			124
# define LEFT			123
# define UP				126
# define DOWN			125
# define ESC			53
# define PLUS			78
# define MINUS			69
# define ENTER_N		76
# define _K				107
# define _L				108
# define _I				105
# define _P				112
# define SPACE			42
# define NB_FRACTAL		6
# define P				-0.5

# define PARAM1			"mandel"
# define PARAM2			"julia"
# define PARAM3			"modulo"
# define PARAM4			"burning"
# define PARAM5			"manta"

# define BAD_ARGS	"./fractol \"args\"(mandel, julia, modulo, burning, manta)"
# define BAD_ALLOC		"Error: malloc() failed"
# define ERROR_INIT		"Error: Bad initialization, retry"
# define ERROR_MLX		"Error: mlx_init() failed"
# define ERROR_WIN		"Error: mlx_new_window() failed"
# define ERROR_IMAGE	"Error: mlx_new_image() failed"
# define ERROR_IMG_STR	"Error: mlx_get_data_addr() failed"
# define PROG_EXIT		"program exit normaly, good bye"
# define PROG_EXIT_FAIL	"Error: program shutdown, you may have some leaks, oups"

# define MOVE			env->move
# define MANDEL 		env->mandel
# define MODULO 		env->modulo
# define KEY			env->key
# define TAB_PARAM		env->tab_param
# define FLAG			env->flag
# define INI			env->ini
# define IMG 			env->img
# define IMAGE 			env->image
# define WIN 			env->win
# define I 				env->i

# define IMG_STR 		env->img->str
# define BPP 			env->img->bpp
# define SIZE			env->img->size
# define ENDIAN 		env->img->endian

# define PIX_JULIA		&env->mandel->pt
# define PIX_MANDEL		&env->mandel->pt

# define M_X			env->mandel->pt.x
# define M_Y			env->mandel->pt.y
# define M_COLOR		env->mandel->pt.color
# define M_COLOR_R		env->mandel->pt.color.r
# define M_COLOR_G		env->mandel->pt.color.g
# define M_COLOR_B		env->mandel->pt.color.b
# define M_ZY			env->mandel->zy
# define M_ZX			env->mandel->zx
# define M_ZX2			env->mandel->zx2
# define M_ZY2			env->mandel->zy2
# define M_CY			env->mandel->cy
# define M_CX			env->mandel->cx
# define M_IT			env->mandel->it

# define MOD_X			env->modulo->pt.x
# define MOD_Y			env->modulo->pt.y
# define MOD_COLOR		env->modulo->pt.color
# define MOD_COLOR_R	env->modulo->pt.color.r
# define MOD_COLOR_G	env->modulo->pt.color.g
# define MOD_COLOR_B	env->modulo->pt.color.b
# define MOD_R			env->modulo->r

# define MV_Y			env->move->mv_y
# define MV_X			env->move->mv_x
# define MV_CY			env->move->mv_cy
# define MV_CX			env->move->mv_cx
# define MV_ZOOM 		env->move->mv_zoom
# define MV_YI			env->move->mv_yi
# define MV_XI			env->move->mv_xi
# define MV_R			env->move->mv_r

# define END			env->key->end
# define MOVE_UP		env->key->move_up
# define MOVE_DOWN		env->key->move_down
# define MOVE_RIGHT		env->key->move_right
# define MOVE_LEFT		env->key->move_left
# define ZOOM_IN		env->key->zoom_in
# define ZOOM_OUT		env->key->zoom_out
# define DEFAULT		env->key->deflt

# define X 				pt->x
# define Y 				pt->y
# define R 				pt->color.r
# define G 				pt->color.g
# define B 				pt->color.b

# define R_				tmp.r
# define G_				tmp.g
# define B_				tmp.b
# define H_				tmp.h
# define S_				tmp.s
# define V_				tmp.v
# define Q_				tmp.q
# define P_				tmp.p
# define T_				tmp.t
# define F_				tmp.f
# define I_				tmp.i

#endif
