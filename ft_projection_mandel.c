/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_projection_mandel.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/04 16:18:06 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 11:50:02 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fractol.h"

static void		ft_calcul(t_env *env)
{
	while (++M_IT < 100 && (M_ZX2 + M_ZY2) < 5.0)
	{
		M_ZY = (2 * M_ZX * M_ZY + M_CY);
		M_ZX = (M_ZX2 - M_ZY2 + M_CX);
		M_ZX2 = M_ZX * M_ZX;
		M_ZY2 = M_ZY * M_ZY;
	}
	if (M_IT == 100)
	{
		M_COLOR = ft_hsv_to_rgb(ft_color_hsv(M_IT % 256, 255, 0));
		ft_pixel_put(env, PIX_MANDEL);
	}
	else
	{
		M_COLOR = ft_hsv_to_rgb(
					ft_color_hsv(M_IT % 256, 255, 255 * (M_IT < 100)));
		ft_pixel_put(env, PIX_MANDEL);
	}
}

int				ft_projection_mandel(t_env *env)
{
	M_Y = -1;
	while (++M_Y < HEIGHT)
	{
		M_X = -1;
		M_CY = (M_Y - MID_HEIGHT) * MV_ZOOM + MV_Y;
		while (++M_X < WIDTH)
		{
			M_CX = (M_X - MID_WIDTH) * MV_ZOOM + MV_X;
			M_ZX = 0.0;
			M_ZY = 0.0;
			M_ZX2 = M_ZX * M_ZX;
			M_ZY2 = M_ZY * M_ZY;
			M_IT = -1;
			ft_calcul(env);
		}
	}
	return (0);
}
