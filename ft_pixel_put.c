/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pixel_put.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/03 12:55:38 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/06 20:02:32 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "define.h"
#include "struct.h"

void			ft_pixel_put(t_env *env, t_point *pt)
{
	int		res;

	res = X * BPP / 8 + Y * SIZE;
	if (X < WIDTH && Y < HEIGHT && X > 0 && Y > 0 && res > 0)
	{
		if (ENDIAN)
		{
			IMG_STR[res] = mlx_get_color_value(INI, R);
			IMG_STR[res + 1] = mlx_get_color_value(INI, G);
			IMG_STR[res + 2] = mlx_get_color_value(INI, B);
		}
		else
		{
			IMG_STR[res] = mlx_get_color_value(INI, B);
			IMG_STR[res + 1] = mlx_get_color_value(INI, G);
			IMG_STR[res + 2] = mlx_get_color_value(INI, R);
		}
	}
}
