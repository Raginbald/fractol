/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 19:04:58 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 13:17:36 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "fractol.h"

static t_mandel	*ft_init_mandel(t_env *env)
{
	t_mandel	*mandel;

	if (!(mandel = (t_mandel *)malloc(sizeof(t_mandel))))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	mandel->pt.x = 0;
	mandel->pt.y = 0;
	mandel->pt.color.r = 0;
	mandel->pt.color.g = 0;
	mandel->pt.color.b = 0;
	mandel->zx = 0;
	mandel->zy = 0;
	mandel->zx2 = 0;
	mandel->zy2 = 0;
	mandel->cx = 0;
	mandel->cy = 0;
	mandel->it = -1;
	return (mandel);
}

static t_modulo	*ft_init_modulo(t_env *env)
{
	t_modulo	*modulo;

	if (!(modulo = (t_modulo *)malloc(sizeof(t_modulo))))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	modulo->pt.x = WIDTH / 2;
	modulo->pt.y = HEIGHT / 2;
	modulo->pt.color.r = 255;
	modulo->pt.color.g = 255;
	modulo->pt.color.b = 255;
	modulo->r = 255;
	return (modulo);
}

static t_img	*ft_init_img(t_env *env)
{
	t_img	*img;

	if (!(img = (t_img *)malloc(sizeof(t_img))))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	img->str = NULL;
	img->bpp = 0;
	img->size = 0;
	img->endian = 0;
	return (img);
}

t_env			*ft_initialization(int ac, char **av)
{
	t_env	*env;

	env = NULL;
	if (ac != 2)
		ft_exit(env, EXIT_FAILURE, 2, BAD_ARGS);
	if (!(env = (t_env *)malloc(sizeof(t_env))))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	env->tab_param = ft_init_tab_param(env);
	ft_check_arg(env, av[1]);
	KEY = ft_init_key(env);
	IMG = ft_init_img(env);
	MOVE = ft_init_move(env);
	MANDEL = ft_init_mandel(env);
	if (TAB_PARAM[0].flag == ON || TAB_PARAM[3].flag == ON
		|| TAB_PARAM[4].flag == ON)
		ft_deflt_mandel(env);
	if (TAB_PARAM[1].flag == ON)
		ft_deflt_julia(env);
	if (TAB_PARAM[2].flag == ON)
		MODULO = ft_init_modulo(env);
	return (env);
}
