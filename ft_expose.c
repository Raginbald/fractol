/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_expose.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 13:39:29 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 11:42:47 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdlib.h>
#include "define.h"
#include "fractol.h"

int		ft_expose(t_env *env)
{
	I = -1;
	mlx_put_image_to_window(INI, WIN, IMAGE, 0, 0);
	while (++I < 3)
	{
		if (TAB_PARAM[I].flag == ON)
			ft_projection(env, TAB_PARAM[I].proj);
	}
	return (0);
}
