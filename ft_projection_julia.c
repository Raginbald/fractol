/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_projection_julia.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 15:55:39 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 11:49:19 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fractol.h"

static void		ft_calcul(t_env *env)
{
	M_IT = -1;
	while (++M_IT < 300)
	{
		M_ZX2 = M_ZX;
		M_ZY2 = M_ZY;
		M_ZY = 2 * M_ZX2 * M_ZY2 + MV_CY;
		M_ZX = M_ZX2 * M_ZX2 - M_ZY2 * M_ZY2 + MV_CX;
		if ((M_ZX * M_ZX + M_ZY * M_ZY) > 4.0)
			break ;
	}
	M_COLOR = ft_hsv_to_rgb(ft_color_hsv(M_IT % 256, 255, 255 * (M_IT < 300)));
	ft_pixel_put(env, PIX_JULIA);
}

int				ft_projection_julia(t_env *env)
{
	M_Y = -1;
	while (++M_Y < HEIGHT)
	{
		M_X = -1;
		while (++M_X < WIDTH)
		{
			M_ZX = 1.5 * (M_X - MID_WIDTH) / (0.5 * MV_ZOOM * WIDTH) + MV_X;
			M_ZY = (M_Y - MID_HEIGHT) / (0.5 * MV_ZOOM * HEIGHT) + MV_Y;
			ft_calcul(env);
		}
	}
	return (0);
}
