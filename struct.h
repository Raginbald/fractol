/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 19:12:51 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 11:59:35 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct s_env		t_env;
typedef struct s_img		t_img;
typedef struct s_key		t_key;
typedef struct s_point		t_point;
typedef struct s_move		t_move;
typedef struct s_rgb		t_rgb;
typedef struct s_hsv		t_hsv;
typedef struct s_tmp		t_tmp;
typedef struct s_param		t_tab_param;
typedef struct s_mandel		t_mandel;
typedef struct s_modulo		t_modulo;
typedef int	(*t_proj)(t_env *);

struct			s_tmp
{
	float		r;
	float		g;
	float		b;
	float		h;
	float		s;
	float		v;
	float		f;
	float		p;
	float		q;
	float		t;
	int			i;
};

struct			s_hsv
{
	float		h;
	float		s;
	float		v;
};

struct			s_param
{
	char		*name;
	char		flag;
	t_proj		proj;
};

struct			s_img
{
	char		*str;
	int			bpp;
	int			size;
	int			endian;
};

struct			s_rgb
{
	int			r;
	int			g;
	int			b;
};

struct			s_point
{
	int			x;
	int			y;
	t_rgb		color;
};

struct			s_mandel
{
	t_point		pt;
	double		zx;
	double		zy;
	double		zx2;
	double		zy2;
	int			it;
	double		cx;
	double		cy;
};

struct			s_modulo
{
	t_point		pt;
	int			r;
};

struct			s_move
{
	double		mv_x;
	double		mv_y;
	double		mv_cx;
	double		mv_cy;
	double		mv_zoom;
	int			mv_xi;
	int			mv_yi;
	int			mv_r;
};

struct			s_key
{
	char		end;
	char		move_up;
	char		move_down;
	char		move_left;
	char		move_right;
	char		zoom_in;
	char		zoom_out;
	char		deflt;
	char		space;
	char		mandel;
};

struct			s_env
{
	void		*ini;
	void		*win;
	void		*image;
	t_img		*img;
	t_tab_param	*tab_param;
	t_key		*key;
	t_mandel	*mandel;
	t_modulo	*modulo;
	t_move		*move;
	int			i;
};

#endif
