/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_projection_modulo.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 11:51:30 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 11:59:55 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fractol.h"

static void		ft_squarre(t_env *env, int x, int y, int l)
{
	t_point	pt;
	int		r;
	int		endy;
	int		endx;

	r = l / 2;
	pt.x = x - l;
	pt.y = y - l;
	endy = y + l;
	endx = x + l;
	pt.color.r = MOD_COLOR_R - y + l;
	pt.color.g = MOD_COLOR_G - x + l;
	pt.color.b = MOD_COLOR_B - x + r;
	while (pt.y < endy)
	{
		pt.x = x - l;
		while (pt.x < endx)
		{
			ft_pixel_put(env, &pt);
			pt.x++;
		}
		pt.y++;
	}
}

static void		ft_star(t_env *env, int x, int y, int r)
{
	if (r > 0)
	{
		ft_star(env, x - r, y - r, r / 2);
		ft_star(env, x + r, y - r, r / 2);
		ft_star(env, x - r, y + r, r / 2);
		ft_star(env, x + r, y + r, r / 2);
		ft_squarre(env, x, y, r);
	}
}

int				ft_projection_modulo(t_env *env)
{
	MOD_X = WIDTH / 2 - MV_XI;
	MOD_Y = HEIGHT / 2 - MV_YI;
	MOD_R = 150 + MV_ZOOM;
	MOD_COLOR_R = 255;
	MOD_COLOR_G = 255;
	MOD_COLOR_B = 255;
	ft_star(env, env->modulo->pt.x, env->modulo->pt.y, env->modulo->r);
	return (0);
}
