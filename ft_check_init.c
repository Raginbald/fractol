/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/01 14:24:34 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/22 17:11:17 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "fractol.h"

void	ft_check_arg(t_env *env, char *arg)
{
	int j;
	int k;

	k = 0;
	j = -1;
	while (++j < NB_FRACTAL)
	{
		if (!ft_strcmp(TAB_PARAM[j].name, arg))
		{
			TAB_PARAM[j].flag = ON;
			k++;
			break ;
		}
	}
	if (k == 0)
		ft_exit(env, EXIT_FAILURE, 2, BAD_ARGS);
}

int		ft_check_init(t_env *env)
{
	if (env && KEY && TAB_PARAM && IMG)
		return (1);
	else
		return (0);
}
