/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_udrl.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/07 17:16:03 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 11:44:21 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "struct.h"

void		ft_key_up(t_env *env)
{
	MV_Y -= (MV_Y - MID_HEIGHT) * MV_ZOOM;
}

void		ft_key_down(t_env *env)
{
	MV_Y += (MV_Y - MID_HEIGHT) * MV_ZOOM;
}

void		ft_key_left(t_env *env)
{
	MV_X -= (MV_X - MID_WIDTH) * MV_ZOOM;
}

void		ft_key_right(t_env *env)
{
	MV_X += (MV_X - MID_WIDTH) * MV_ZOOM;
}
