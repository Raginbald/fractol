/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_loop_hook.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 18:17:35 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 10:27:17 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <stdlib.h>
#include "define.h"
#include "fractol.h"

int		ft_loop_hook(t_env *env)
{
	mlx_put_image_to_window(INI, WIN, IMAGE, 0, 0);
	if (END)
		ft_key_esc(env);
	if (MOVE_UP)
		ft_key_up(env);
	if (MOVE_DOWN)
		ft_key_down(env);
	if (MOVE_RIGHT)
		ft_key_right(env);
	if (MOVE_LEFT)
		ft_key_left(env);
	if (DEFAULT)
		ft_key_deflt(env);
	I = -1;
	while (++I < NB_FRACTAL)
	{
		if (TAB_PARAM[I].flag == ON)
			ft_projection(env, TAB_PARAM[I].proj);
	}
	mlx_put_image_to_window(INI, WIN, IMAGE, 0, 0);
	mlx_do_sync(INI);
	return (0);
}
