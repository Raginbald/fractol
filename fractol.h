/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 20:08:46 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 12:34:55 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "struct.h"

t_env		*ft_initialization(int ac, char **av);

t_tab_param	*ft_init_tab_param(t_env *env);
t_move		*ft_init_move(t_env *env);
t_key		*ft_init_key(t_env *env);
int			ft_check_init(t_env *env);
void		ft_check_arg(t_env *env, char *arg);

int			ft_loop_hook(t_env *env);
int			ft_expose(t_env *env);

void		ft_projection(t_env *env, int (*f)(t_env *));
int			ft_projection_burning_ship(t_env *env);
int			ft_projection_manta(t_env *env);
int			ft_projection_mandel(t_env *env);
int			ft_projection_julia(t_env *env);
int			ft_projection_modulo(t_env *env);

void		ft_pixel_put(t_env *env, t_point *pt);
t_hsv		ft_color_hsv(int h, int s, int v);
t_rgb		ft_hsv_to_rgb(t_hsv hsv);

int			ft_pointer_moved(int x, int y, t_env *env);
int			ft_button_press(int button, int x, int y, t_env *env);
int			ft_scroll_press(int button, int x, int y, t_env *env);
int			ft_key_press(int key, t_env *env);
int			ft_key_release(int key, t_env *env);

void		ft_key_esc(t_env *env);
void		ft_key_deflt(t_env *env);
void		ft_key_up(t_env *env);
void		ft_key_down(t_env *env);
void		ft_key_left(t_env *env);
void		ft_key_right(t_env *env);
void		ft_wheel_zoom_in(t_env *env, int x, int y);
void		ft_wheel_zoom_out(t_env *env, int x, int y);
void		ft_button_zoom_in(t_env *env, int x, int y);
void		ft_button_zoom_out(t_env *env, int x, int y);

void		ft_move_julia(t_env *env, double nb, int x, int y);
void		ft_move_mandel(t_env *env, double nb, int x, int y);

void		ft_deflt_mandel(t_env *env);
void		ft_deflt_julia(t_env *env);
void		ft_exit(t_env *env, int flag, int fd, char *exit_str);

#endif
