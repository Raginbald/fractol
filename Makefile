# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/16 13:48:28 by graybaud          #+#    #+#              #
#    Updated: 2015/03/23 12:36:06 by graybaud         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME		= fractol
DEBUG		= debug_version_$(NAME)
LIBFT_DIR	= libft
LIBFT		= $(LIBFT_DIR)/libft.a
MINILIBX_DIR= mlx
MINILIBX	= $(MINILIBX_DIR)/libmlx.a
CC			= gcc
LFLAGS		= -L ./libft -lft -L ./mlx -lmlx -framework OpenGL -framework AppKit
CFLAGS		= -Wall -Wextra -Werror
IFLAGS		= -I ./libft/ -I ./mlx/ -I./
DFLAGS		= -g -DD_ERROR_ON
RM			= rm -Rf
H			= 	fractol.h 		\
				struct.h 		\
				define.h

SRC			= 	main.c				\
				ft_init.c			\
				ft_init_env.c		\
				ft_check_init.c 	\
				ft_exit.c 			\
				ft_press_release.c 	\
				ft_color.c 			\
				ft_loop_hook.c 		\
				ft_expose.c 		\
				ft_pixel_put.c 		\
				ft_key_zoom.c 		\
				ft_move.c 			\
				ft_projection.c 	\
				ft_projection_burning_ship.c\
				ft_projection_manta.c\
				ft_projection_mandel.c 	\
				ft_projection_julia.c 	\
				ft_projection_modulo.c 	\
				ft_key_control.c 	\
				ft_key_udrl.c 			\
				ft_pointer_moved.c 		\

OBJ			= $(SRC:.c=.o)

$(NAME): $(MINILIBX) $(LIBFT) $(OBJ) $(H)
	@echo "building $(NAME) ... "
	@$(CC) $(CFLAGS) -o $@ $(OBJ) $(IFLAGS) $(LFLAGS)
	@echo "$(NAME) created ! \n"
	@echo "building $(DEBUG) ... "
	@$(CC) $(CFLAGS) $(DFLAGS) $(SRC) -o $(DEBUG) $(IFLAGS) $(LFLAGS)
	@echo "$(DEBUG) created !"

%.o: %.c
	@$(CC) $(CFLAGS) $(IFLAGS) -c $^ -o $@

$(LIBFT):
	@echo "building $(LIBFT_DIR) ... "
	@$(MAKE) -C ./$(LIBFT_DIR)
	@echo "$(LIBFT_DIR) created ! \n"

$(MINILIBX):
	@echo "building $(MINILIBX_DIR) ... "
	@($(MAKE) -C ./$(MINILIBX_DIR))
	@echo "$(MINILIBX_DIR) created ! \n"

all:$(NAME)

clean:
	@$(RM) $(DEBUG) $(DEBUG).dSYM $(OBJ)
	@(cd  $(LIBFT_DIR) && $(MAKE) $@)
	@(cd  $(MINILIBX_DIR) && $(MAKE) $@)
	@echo "$(DEBUG), $(DEBUG).dSYM, objects erased !"

fclean:clean
	@$(RM) $(NAME)
	@(cd  $(LIBFT_DIR) && $(MAKE) $@)
	@echo "$(NAME), $(LIBFT_DIR) erased !\n"

re: fclean all

.PHONY:all clean fclean re
