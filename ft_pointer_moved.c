/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pointer_moved.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 18:18:16 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 13:20:08 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "struct.h"

int ft_pointer_moved(int x, int y, t_env *env)
{
	if (x > WIDTH)
		x = WIDTH;
	if (x < 0)
		x = 0;
	if (y > WIDTH)
		y = WIDTH;
	if (y < 0)
		y = 0;
	MV_CX = ((double)x * PIX_WIN - 2) / 2.2;
	MV_CY = ((double)y * PIX_WIN - 2) / 2.2;
	return (0);
}
