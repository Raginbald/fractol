/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_color.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/19 17:28:24 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 12:03:02 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "define.h"
#include "struct.h"

t_hsv		ft_color_hsv(int h, int s, int v)
{
	t_hsv	color;

	color.h = h;
	color.s = s;
	color.v = v;
	return (color);
}

static void	ft_zero_saturation(float *r, float *g, float *b, float *v)
{
	r = v;
	g = v;
	b = v;
}

static void ft_test_i_next(t_tmp *tmp)
{
	if (tmp->i == 4)
	{
		tmp->r = tmp->t;
		tmp->g = tmp->p;
		tmp->b = tmp->v;
	}
	if (tmp->i == 5)
	{
		tmp->r = tmp->v;
		tmp->g = tmp->p;
		tmp->b = tmp->q;
	}
}

static void ft_test_i_(t_tmp *tmp)
{
	if (tmp->i == 0)
	{
		tmp->r = tmp->v;
		tmp->g = tmp->t;
		tmp->b = tmp->p;
	}
	if (tmp->i == 1)
	{
		tmp->r = tmp->q;
		tmp->g = tmp->v;
		tmp->b = tmp->p;
	}
	if (tmp->i == 2)
	{
		tmp->r = tmp->p;
		tmp->g = tmp->v;
		tmp->b = tmp->t;
	}
	if (tmp->i == 3)
	{
		tmp->r = tmp->p;
		tmp->g = tmp->q;
		tmp->b = tmp->v;
	}
	ft_test_i_next(tmp);
}

t_rgb		ft_hsv_to_rgb(t_hsv hsv)
{
	t_rgb	rgb;
	t_tmp	tmp;

	rgb.r = 0;
	rgb.g = 0;
	rgb.b = 0;
	H_ = hsv.h / 256.0;
	S_ = hsv.s / 256.0;
	V_ = hsv.v / 256.0;
	if (S_ == 0)
		ft_zero_saturation(&R_, &G_, &B_, &V_);
	else
	{
		H_ *= 6;
		I_ = (int)(floor(H_));
		F_ = H_ - I_;
		P_ = V_ * (1 - S_);
		Q_ = V_ * (1 - (S_ * F_));
		T_ = V_ * (1 - (S_ * (1 - F_)));
	}
	ft_test_i_(&tmp);
	rgb.r = (int)(R_ * 255.0);
	rgb.g = (int)(G_ * 255.0);
	rgb.b = (int)(B_ * 255.0);
	return (rgb);
}
