/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 15:50:37 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 13:00:30 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <libft.h>
#include <stdlib.h>
#include "define.h"
#include "fractol.h"

int		main(int ac, char **av)
{
	t_env	*env;

	env = ft_initialization(ac, av);
	if (!(INI = mlx_init()))
		ft_exit(env, EXIT_FAILURE, 2, ERROR_MLX);
	if (!(WIN = mlx_new_window(INI, WIDTH, HEIGHT, "FRACTOL")))
		ft_exit(env, EXIT_FAILURE, 2, ERROR_WIN);
	if (!(IMAGE = mlx_new_image(INI, WIDTH, HEIGHT)))
		ft_exit(env, EXIT_FAILURE, 2, ERROR_IMAGE);
	if (!(IMG_STR = mlx_get_data_addr(IMAGE, &BPP, &SIZE, &ENDIAN)))
		ft_exit(env, EXIT_FAILURE, 2, ERROR_IMG_STR);
	mlx_hook(WIN, 6, 6, ft_pointer_moved, env);
	mlx_hook(WIN, 4, 4, ft_button_press, env);
	mlx_hook(WIN, 2, 3, ft_key_press, env);
	mlx_hook(WIN, 3, 1, ft_key_release, env);
	mlx_loop_hook(INI, ft_loop_hook, env);
	mlx_loop(INI);
	return (0);
}
