/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_zoom.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/07 17:04:56 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 12:08:16 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "fractol.h"

void		ft_wheel_zoom_out(t_env *env, int x, int y)
{
	if (TAB_PARAM[0].flag == ON || TAB_PARAM[3].flag == ON
		|| TAB_PARAM[4].flag == ON)
		ft_move_mandel(env, 2.0, x, y);
	if (TAB_PARAM[1].flag == ON)
		ft_move_julia(env, 1.2, x, y);
}

void		ft_wheel_zoom_in(t_env *env, int x, int y)
{
	if (TAB_PARAM[0].flag == ON || TAB_PARAM[3].flag == ON
		|| TAB_PARAM[4].flag == ON)
		ft_move_mandel(env, 0.6, x, y);
	if (TAB_PARAM[1].flag == ON)
		ft_move_julia(env, 0.3, x, y);
}

void		ft_button_zoom_out(t_env *env, int x, int y)
{
	if (TAB_PARAM[0].flag == ON || TAB_PARAM[3].flag == ON
		|| TAB_PARAM[4].flag == ON)
		ft_move_mandel(env, 2.0, x, y);
	if (TAB_PARAM[1].flag == ON)
		ft_move_julia(env, 0.3, x, y);
}

void		ft_button_zoom_in(t_env *env, int x, int y)
{
	if (TAB_PARAM[0].flag == ON || TAB_PARAM[3].flag == ON
		|| TAB_PARAM[4].flag == ON)
		ft_move_mandel(env, 0.6, x, y);
	if (TAB_PARAM[1].flag == ON)
		ft_move_julia(env, 1.2, x, y);
}
