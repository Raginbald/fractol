/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_move.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 10:30:19 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 10:30:41 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "define.h"
#include "struct.h"

void		ft_move_mandel(t_env *env, double nb, int x, int y)
{
	MV_ZOOM = MV_ZOOM * nb;
	MV_X += (x - MID_WIDTH) * MV_ZOOM;
	MV_Y += (y - MID_HEIGHT) * MV_ZOOM;
}

void		ft_move_julia(t_env *env, double nb, int x, int y)
{
	MV_ZOOM = MV_ZOOM * nb;
	(void)x;
	(void)y;
}
