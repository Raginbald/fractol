/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/31 16:01:18 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 12:58:55 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include "define.h"
#include "fractol.h"

t_tab_param	*ft_init_tab_param(t_env *env)
{
	t_tab_param	*tab;

	if (!(tab = (t_tab_param *)malloc(sizeof(t_tab_param) * NB_FRACTAL)))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	tab[0].name = ft_strdup(PARAM1);
	tab[0].flag = OFF;
	tab[0].proj = ft_projection_mandel;
	tab[1].name = ft_strdup(PARAM2);
	tab[1].flag = OFF;
	tab[1].proj = ft_projection_julia;
	tab[2].name = ft_strdup(PARAM3);
	tab[2].flag = OFF;
	tab[2].proj = ft_projection_modulo;
	tab[3].name = ft_strdup(PARAM4);
	tab[3].flag = OFF;
	tab[3].proj = ft_projection_burning_ship;
	tab[4].name = ft_strdup(PARAM5);
	tab[4].flag = OFF;
	tab[4].proj = ft_projection_manta;
	tab[5].name = ft_strdup("");
	tab[5].flag = OFF;
	return (tab);
}

t_move		*ft_init_move(t_env *env)
{
	t_move	*move;

	if (!(move = (t_move *)malloc(sizeof(t_move))))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	move->mv_x = 0;
	move->mv_y = 0;
	move->mv_xi = 0;
	move->mv_yi = 0;
	move->mv_r = 0;
	move->mv_zoom = 0;
	return (move);
}

t_key		*ft_init_key(t_env *env)
{
	t_key	*key;

	if (!(key = (t_key *)malloc(sizeof(t_key))))
		ft_exit(env, EXIT_FAILURE, 2, BAD_ALLOC);
	return (key);
}
