/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_key_control.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: graybaud <graybaud@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/30 18:50:02 by graybaud          #+#    #+#             */
/*   Updated: 2015/03/23 13:21:25 by graybaud         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "define.h"
#include "fractol.h"

void	ft_key_esc(t_env *env)
{
	if (env)
		ft_exit(env, EXIT_SUCCESS, 1, PROG_EXIT);
	else
		ft_exit(env, EXIT_FAILURE, 2, PROG_EXIT_FAIL);
}

void	ft_deflt_mandel(t_env *env)
{
	MV_X = 0;
	MV_Y = 0;
	MV_ZOOM = 1.0 / 256;
}

void	ft_deflt_julia(t_env *env)
{
	MV_X = 0;
	MV_Y = 0;
	MV_ZOOM = 1.0;
	MV_CX = 0.285;
	MV_CY = 0.01;
}

void	ft_deflt_modulo(t_env *env)
{
	MV_XI = 0;
	MV_YI = 0;
	MV_ZOOM = 1;
}

void	ft_key_deflt(t_env *env)
{
	if (TAB_PARAM[0].flag == ON || TAB_PARAM[3].flag == ON
		|| TAB_PARAM[4].flag == ON)
		ft_deflt_mandel(env);
	if (TAB_PARAM[1].flag == ON)
		ft_deflt_julia(env);
	if (TAB_PARAM[2].flag == ON)
		ft_deflt_modulo(env);
}
